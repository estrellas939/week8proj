# Rust Command-Line Tool with Testing

This Rust CLI tool is called `domath`，it can process numbers in a file and calculate the arithmetic mean, median, geometric mean, and harmonic mean.

## Part I. Basic Environment Set-up
### 1. Set Rust Environment
Install Cargo.

### 2. Create the Project Directory
Open a terminal and run `cargo new week8proj` to create a new rust project, and run `cd week8proj` to enter the directory.

### 3. Add Dependencies
Under the file `Cargo.toml`, add all necessery dependencies:
```
[dependencies]
clap = { version = "3.0.0-beta.1", package = "clap-v3" }
clap_derive-v3 = "3.0.0-beta.1"
```
Here we need to clarify that the latest version of `clap` pkg is 4.5.3. However, due to `clap` pkg embedded `App` struct after 4.X version, we choose an older version here with `App` struct available.

## Part II. CLI Tool Functionality and Data Processing
### 4. Tool functionality
Under `src/main.rs` file, we use clap to parse command line parameters and get the file name. Also adding some error handlings.

```
let matches = App::new("domath")
        .version("1.0")
        .author("George Wang")
        .about("Does awesome math operations on a list of numbers")
        .arg(Arg::new("filename")
             .help("The file containing numbers, separated by commas")
             .required(true)
             .index(1))
        .get_matches();

    let filename = matches.value_of("filename").unwrap();
    let contents = match fs::read_to_string(filename) {
        Ok(contents) => contents,
        Err(e) => {
            eprintln!("Error reading file: {}", e);
            return;
        }
    };    
```
So basically this CLI tool's format is `domath {filename}`.

### 5. Data processing
The data read from the file is a string and we need to convert it to an array of numbers. Then, we can do calculations on these numbers.

```
let mut numbers: Vec<f64> = Vec::new();
    for n in contents.trim().split(',') {
        match n.parse() {
            Ok(num) => numbers.push(num),
            Err(_) => eprintln!("Warning: Non-numeric value encountered: {}", n),
        }
    }

    if numbers.is_empty() {
        println!("No valid numbers provided.");
        return;
    }
```
Here we specify the numbers are separated by commas.

Then we need to define each functions' functionality:
```
fn arithmetic_mean(numbers: &[f64]) -> f64 {
    numbers.iter().sum::<f64>() / numbers.len() as f64
}

fn median(numbers: &mut [f64]) -> f64 {
    numbers.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
    let mid = numbers.len() / 2;
    if numbers.len() % 2 == 0 {
        (numbers[mid - 1] + numbers[mid]) / 2.0
    } else {
        numbers[mid]
    }
}

fn geometric_mean(numbers: &[f64]) -> f64 {
    let product: f64 = numbers.iter().product();
    product.powf(1.0 / numbers.len() as f64)
}

fn harmonic_mean(numbers: &[f64]) -> f64 {
    let sum: f64 = numbers.iter().map(|&n| 1.0 / n).sum();
    numbers.len() as f64 / sum
}
```

### 6. Results Display
Once all calculation functions are defined, we can call them in the main function and display the results.
```
println!("Arithmetic mean: {}", arithmetic_mean(&numbers));
    println!("Median: {}", median(&mut numbers));

    if numbers.iter().any(|&n| n <= 0.0) {
        println!("Cannot calculate geometric and harmonic means for non-positive numbers.");
    } else {
        println!("Geometric mean: {}", geometric_mean(&numbers));
        println!("Harmonic mean: {}", harmonic_mean(&numbers));
    }
```

## Part III. Test
### 7. Testing implementation
Then we need to write unit tests for the calculation functions.
```
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_arithmetic_mean() {
        let numbers = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        assert_eq!(arithmetic_mean(&numbers), 3.0);
    }

    #[test]
    fn test_median() {
        let mut numbers = vec![5.0, 1.0, 4.0, 2.0, 3.0];
        assert_eq!(median(&mut numbers), 3.0);
    }

    #[test]
    fn test_geometric_mean() {
        let numbers = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        assert!((geometric_mean(&numbers) - 2.605171084697352).abs() < 1e-9);
    }

    #[test]
    fn test_harmonic_mean() {
        let numbers = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        assert!((harmonic_mean(&numbers) - 2.18978102189781).abs() < 1e-9);
    }
}
```
We can add more test cases.

### 8. Testing and Debugging
In the terminal, run `cargo test` to test the function. And we shall see the following result:
```
   Compiling week8proj v0.1.0 (/Users/wanghaotian/week8proj)
    Finished test [unoptimized + debuginfo] target(s) in 2.45s
     Running unittests src/main.rs (target/debug/deps/week8proj-47aac181a2065585)

running 4 tests
test tests::test_arithmetic_mean ... ok
test tests::test_harmonic_mean ... ok
test tests::test_geometric_mean ... ok
test tests::test_median ... ok

test result: ok. 4 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
```
![screenshot of test](https://gitlab.com/estrellas939/week8proj/-/raw/main/pic/cargotest.png?ref_type=heads&inline=false)

### 9. CLI Tool Release
First we add `[bin]` in the `Cargo.toml` file to build a executable file with our expected name and path.
```
[[bin]]
name = "domath" 
path = "src/main.rs"
```

Then we run `cargo build --release` to build this executable file.
```
   Compiling week8proj v0.1.0 (/Users/wanghaotian/week8proj)
    Finished release [optimized] target(s) in 0.97s
```
Now the executable file `doamth.d` has been created and is ready for being called in the terminal.

### 10. Final Test
Before the terminal test, we need to create a test file called `test.txt`.
```
1,2,3,4,5
```
Then, we just need to run `./target/release/domath test.txt` and wait for the results:
```
Arithmetic mean: 3
Median: 3
Geometric mean: 2.605171084697352
Harmonic mean: 2.18978102189781
```
![screenshot of file test](https://gitlab.com/estrellas939/week8proj/-/raw/main/pic/cargobuildrelease.png?ref_type=heads&inline=false)

We can also download all the assets under `Deploy/Releases` section from left navigation bar.