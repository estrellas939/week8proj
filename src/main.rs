use clap::{App, Arg};
use std::fs;

fn main() {
    let matches = App::new("domath")
        .version("1.0")
        .author("George Wang")
        .about("Does awesome math operations on a list of numbers")
        .arg(Arg::new("filename")
             .help("The file containing numbers, separated by commas")
             .required(true)
             .index(1))
        .get_matches();

    let filename = matches.value_of("filename").unwrap();
    let contents = match fs::read_to_string(filename) {
        Ok(contents) => contents,
        Err(e) => {
            eprintln!("Error reading file: {}", e);
            return;
        }
    };

    let mut numbers: Vec<f64> = Vec::new();
    for n in contents.trim().split(',') {
        match n.parse() {
            Ok(num) => numbers.push(num),
            Err(_) => eprintln!("Warning: Non-numeric value encountered: {}", n),
        }
    }

    if numbers.is_empty() {
        println!("No valid numbers provided.");
        return;
    }

    println!("Arithmetic mean: {}", arithmetic_mean(&numbers));
    println!("Median: {}", median(&mut numbers));

    if numbers.iter().any(|&n| n <= 0.0) {
        println!("Cannot calculate geometric and harmonic means for non-positive numbers.");
    } else {
        println!("Geometric mean: {}", geometric_mean(&numbers));
        println!("Harmonic mean: {}", harmonic_mean(&numbers));
    }
}

fn arithmetic_mean(numbers: &[f64]) -> f64 {
    numbers.iter().sum::<f64>() / numbers.len() as f64
}

fn median(numbers: &mut [f64]) -> f64 {
    numbers.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
    let mid = numbers.len() / 2;
    if numbers.len() % 2 == 0 {
        (numbers[mid - 1] + numbers[mid]) / 2.0
    } else {
        numbers[mid]
    }
}

fn geometric_mean(numbers: &[f64]) -> f64 {
    let product: f64 = numbers.iter().product();
    product.powf(1.0 / numbers.len() as f64)
}

fn harmonic_mean(numbers: &[f64]) -> f64 {
    let sum: f64 = numbers.iter().map(|&n| 1.0 / n).sum();
    numbers.len() as f64 / sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_arithmetic_mean() {
        let numbers = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        assert_eq!(arithmetic_mean(&numbers), 3.0);
    }

    #[test]
    fn test_median() {
        let mut numbers = vec![5.0, 1.0, 4.0, 2.0, 3.0];
        assert_eq!(median(&mut numbers), 3.0);
    }

    #[test]
    fn test_geometric_mean() {
        let numbers = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        assert!((geometric_mean(&numbers) - 2.605171084697352).abs() < 1e-9);
    }

    #[test]
    fn test_harmonic_mean() {
        let numbers = vec![1.0, 2.0, 3.0, 4.0, 5.0];
        assert!((harmonic_mean(&numbers) - 2.18978102189781).abs() < 1e-9);
    }
}
